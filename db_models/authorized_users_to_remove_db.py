from app import db


class AuthorizedUsersToRemove(db.Model):
    def __init__(self):
        pass

    id = db.Column(db.Integer, primary_key=True)
    authorized_user_id = db.Column(db.Integer, unique=True, nullable=False)