from db_models.authorized_users_to_remove_db import AuthorizedUsersToRemove


class AuthorizationDAL:
    def __init__(self):
        pass

    def is_authorized_to_remove(self, id):
        return AuthorizedUsersToRemove.query.filter_by(id=id).first() is not None
