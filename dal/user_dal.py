from db_models.user_db import User
from db_models.user_db import db

class UserDal:
    def __init__(self):
        pass

    def get_user(self, username):
        return User.query.filter_by(username=username).first()

    def add_user(self, username, password, job):
        user = User(username=username, password=password, job=job)
        db.session.add(user)
        db.session.commit()

    def remove_user(self, username):
        User.query.filter_by(username=username).delete()
        db.session.commit()
